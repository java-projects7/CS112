import java.io.Serializable;

public class Comic implements Serializable
{
	//CONSTANTS
	public static final char MARVEL = 'M';
	public static final char DC = 'D';
	public static final char IMAGE = 'I';
	public static final char VERTIGO = 'V';
	public static final char NONE = 'N';
	public static final double RATING_MIN = 0.0;
	public static final double RATING_MAX = 10.0;
	
	public static final String DEFAULT_NAME = "Blank";
	public static final int DEFAULT_ISSUE_NUM = 0;
	public static final char DEFAULT_PUBLISHER = MARVEL;
	public static final double DEFAULT_RATING = RATING_MIN;
	
	//INSTANCE VARIABLES
	private String name;
	private int issueNum; //0 and up
	private char publisher;
	private double rating; //0.0 - 10.0 inclusive for quality of comic
	
	//CONSTRUCTORS
	public Comic(String name, int issueNum, char publisher, double rating)
	{
		if( !setAll(name, issueNum, publisher, rating) )
		{ 
			//consider upgrading this with exceptions
			System.out.println("ERROR: bad data in constructor");
			System.out.println("Exiting...please run again");
			System.exit(0);
		}
	}
	
	public Comic()
	{
		this(DEFAULT_NAME, DEFAULT_ISSUE_NUM, DEFAULT_PUBLISHER, DEFAULT_RATING);
	}
	
	public Comic(Comic original)
	{
		if(original == null ||
			!setAll(original.name, original.issueNum, original.publisher, original.rating) )
		{ 
			//consider upgrading this with exceptions
			System.out.println("ERROR: bad data in copy constructor");
			System.out.println("Exiting...please run again");
			System.exit(0);
		}
	}
	
	public Comic(String csv) //useful for CSV file input!
	{
		String[] parts = csv.split(",");
		String name;
		int issueNum;
		char publisher;
		double rating;
		
		if(parts.length != 4)
		{
			//consider upgrading this with exceptions
			System.out.println("ERROR: incomplete data line given to CSV constructor");
			System.out.println("Exiting...please run again");
			System.exit(0);
		}
		
		name = parts[0];
		issueNum = Integer.parseInt(parts[1]);
		publisher = parts[2].charAt(0);
		rating = Double.parseDouble(parts[3]);
		
		if( !setAll(name, issueNum, publisher, rating) )
		{ 
			//consider upgrading this with exceptions
			System.out.println("ERROR: bad data in CSV constructor");
			System.out.println("Exiting...please run again");
			System.exit(0);
		}
		
	}
	
	//SETTERS
	public void setName(String name)
	{
		this.name = name;
	}
	
	public boolean setIssueNum(int issueNum)
	{
		if(issueNum < 0)
		{
			return false;
		}
		else
		{
			this.issueNum = issueNum;
			return true;
		}
	}
	
	public boolean setPublisher(char publisher)
	{
		if(publisher != MARVEL && publisher != DC &&
			publisher != IMAGE && publisher != VERTIGO)
		{
			return false;
		}
		else
		{
			this.publisher = publisher;
			return true;
		}
	}
	
	public boolean setRating(double rating)
	{
		if(rating < RATING_MIN || rating > RATING_MAX)
		{
			return false;
		}
		else
		{
			this.rating = rating;
			return true;
		}
	}
	
	public boolean setAll(String name, int issueNum, char publisher, double rating)
	{
		//call void methods first, no error checking here
		setName(name);
		//return results of boolean methods that error check
		return setIssueNum(issueNum) && setPublisher(publisher) && setRating(rating);
	}
	
	//GETTERS
	public String getName()
	{
		return this.name;
	}
	
	public int getIssueNum()
	{
		return this.issueNum;
	}
	
	public char getPublisher()
	{
		return this.publisher;
	}
	
	public double getRating()
	{
		return this.rating;
	}
	
	//OTHER REQUIRED
	public boolean equals(Object other)
	{
		Comic otherComic;
		
		if(other == null || other.getClass() != this.getClass())
		{
			return false;
		}
		else
		{
			otherComic = (Comic)other;
			return otherComic.name.equals(this.name) &&
					otherComic.issueNum == this.issueNum &&
					otherComic.publisher == this.publisher &&
					otherComic.rating == this.rating;
		}
	}
	
	public String toString()
	{
		return String.format("%s #%d (%s), Rating: %.1f/%.1f",
			this.name, this.issueNum, publisherToString(this.publisher),
			this.rating, RATING_MAX);
	}
	
	public String toCSV() //useful for text file output!
	{
		return String.format("%s,%d,%c,%.1f",
			this.name, this.issueNum, this.publisher, this.rating);
	}
	
	
	//HELPER METHODS
	private static String publisherToString(char publisher)
	{
		String result;
		
		switch(publisher)
		{
			case MARVEL:
				result = "Marvel";
				break;
			case DC:
				result = "DC";
				break;
			case IMAGE:
				result = "Image";
				break;
			case VERTIGO:
				result = "Vertigo";
				break;
			default:
				result = "?";
				break;
		}
		
		return result;
	}
	
	
}