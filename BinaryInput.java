/* 
//BASIC_DATA_FILE format:
	// 1 integer representing number of each type of data file has (n)
	// n integer's representing comic issue number
	// n string's representing comic name
	// n char's representing comic publisher
	// n double's representing comic quality rating
//ARRAY_DATA_FILE format:
	// array of integer's representing comic issue numbers
	// array of string's representing comic names
	// array of char's representing comic publishers
	// array of double's representing comic quality ratings
//OBJECT_DATA_FILE format:
	// 1 integer representing number of comic objects file has (n)
	// n Comic object's
//OBJECT_ARRAY_DATA_FILE format:
	// array of Comic object's
*/

import java.io.ObjectInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.FileNotFoundException;
import java.lang.NumberFormatException;
import java.util.Scanner;

public class BinaryInput
{
	public static final String BASIC_DATA_FILE = "basicData.dat";
	public static final String ARRAY_DATA_FILE = "arrayData.dat";
	public static final String OBJECT_DATA_FILE = "objectData.dat";
	public static final String OBJECT_ARRAY_DATA_FILE = "objectArrayData.dat";
	
	public static final int MAX_ARRAY_SIZE = 100;
	public static void main(String[] args)
    {
		//DECLARATIONS + INITIALIZATIONS
		Scanner keyboard = new Scanner(System.in);
		int choice, numComics;
		Comic[] collection = new Comic[MAX_ARRAY_SIZE];
		int[] issueNums = new int[MAX_ARRAY_SIZE];
		String[] names = new String[MAX_ARRAY_SIZE];
		char[] publishers = new char[MAX_ARRAY_SIZE];
		double[] ratings = new double[MAX_ARRAY_SIZE];
		String fileName = ""; //keep compiler happy
		
		System.out.println("Welcome to Binary File Input!");

		do
		{
			//INPUT
			System.out.println("\n\n");
			System.out.println("What kind of data format is the file in?");
			System.out.println("1: Basic data file (primitives + Strings)");
			System.out.println("2: Array data file (arrays of primitives + Strings)");
			System.out.println("3: Object data file (objects)");
			System.out.println("4: Object array data file (array of objects)");
			System.out.println("5: Exit");
			choice = readInt("Enter choice: ", keyboard, 1, 5);
			
			//PROCESSING + (console/file)OUTPUT
			if(choice != 5)
			{
				System.out.print("Please input file name to read in: ");
				fileName = keyboard.nextLine();
			}
			System.out.println();
			
			switch(choice)
			{
				case 1:
					numComics = inputBasicData(fileName, issueNums, names, publishers, ratings);
					System.out.println("Data read from " + fileName + ":");
					printArray(numComics, issueNums, names, publishers, ratings);
					break;
				case 2:
					numComics = inputArrayData(fileName, issueNums, names, publishers, ratings);
					System.out.println("Data read from " + fileName + ":");
					printArray(numComics, issueNums, names, publishers, ratings);
					break;
				case 3:
					numComics = inputObjectData(fileName, collection);
					System.out.println("Data read from " + fileName + ":");
					printArray(numComics, collection);
					break;
				case 4:
					numComics = inputObjectArrayData(fileName, collection);
					System.out.println("Data read from " + fileName + ":");
					printArray(numComics, collection);
					break;
				case 5:
					System.out.println("Thanks for using our program! See you soon!");
					break;
				//no default needed, readInt() guarantees we get a value 1-5
			}
			
			System.out.println();
			
		} while (choice != 5);
		
		keyboard.close(); //housekeeping
    }
    
    
    
    //BINARY FILE OUTPUT METHODS
    
    public static int inputBasicData(String inputFileName, 
		int[] issueNums, String[] names, char[] publishers, double[] ratings)
    {
		//initializations to keep compiler happy
		int n = 0;
		ObjectInputStream inputStream = null;
		
		try
        {
            inputStream = new ObjectInputStream(new FileInputStream(inputFileName));

			//BASIC_DATA_FILE format:
			// 1 integer representing number of each type of data file has (n)
			n = inputStream.readInt();
			
			// n integer's representing comic issue number
			for(int i = 0; i < n; i++)
			{
				issueNums[i] = inputStream.readInt();
			}
			// n string's representing comic name
			for(int i = 0; i < n; i++)
			{
				names[i] = inputStream.readUTF();
			}
			// n char's representing comic publisher
			for(int i = 0; i < n; i++)
			{
				publishers[i] = (char)inputStream.readChar();
			}
			// n double's representing comic quality rating
			for(int i = 0; i < n; i++)
			{
				ratings[i] = inputStream.readDouble();
			}
			
            inputStream.close();
        }
        catch (FileNotFoundException e)
        {
            System.out.println("ERROR: cannot find file " + inputFileName);
            System.out.println("Ending program...");
            System.exit(0);
        }
        catch (IOException e)
        {
            System.out.println("ERROR: problem with file input from " + inputFileName);
            System.out.println("Ending program...");
            System.exit(0);
        }
        
        return n;
	}
	
	public static int inputArrayData(String inputFileName,
		int[] issueNums, String[] names, char[] publishers, double[] ratings)
	{
		//initializations to keep compiler happy
		int n = 0;
		ObjectInputStream inputStream = null;
				
		try
        {
            inputStream = new ObjectInputStream(new FileInputStream(inputFileName));
			Object array;

			//ARRAY_DATA_FILE format:
			// array of integer's representing comic issue numbers
			array = inputStream.readObject();
			n = ((int[])array).length; //get size of array, only need to do once
			for(int i = 0; i < n; i++)
			{
				issueNums[i] = ((int[])array)[i];
			}
			// array of string's representing comic names
			array = inputStream.readObject();
			for(int i = 0; i < n; i++)
			{
				names[i] = ((String[])array)[i];
			}
			// array of char's representing comic publishers
			array = inputStream.readObject();
			for(int i = 0; i < n; i++)
			{
				publishers[i] = ((char[])array)[i];
			}
			// array of double's representing comic quality ratings
			array = inputStream.readObject();
			for(int i = 0; i < n; i++)
			{
				ratings[i] = ((double[])array)[i];
			}
			
            inputStream.close();
        }
        catch (ClassNotFoundException e)
        {
			System.out.println("ERROR: cannot read object from " + inputFileName);
			System.out.println("Ending program...");
            System.exit(0);
		}
        catch (FileNotFoundException e)
        {
            System.out.println("ERROR: cannot find file " + inputFileName);
            System.out.println("Ending program...");
            System.exit(0);
        }
        catch (IOException e)
        {
            System.out.println("ERROR: problem with file input from " + inputFileName);
            System.out.println("Ending program...");
            System.exit(0);
        }
		
		return n;
	}
	
	public static int inputObjectData(String inputFileName, Comic[] comics)
	{
		//initializations to keep compiler happy
		int n = 0;
		ObjectInputStream inputStream = null;
		
		try
        {
            inputStream = new ObjectInputStream(new FileInputStream(inputFileName));

			//OBJECT_DATA_FILE format:
			// 1 integer representing number of comic objects file has (n)
			n = inputStream.readInt();
			// n Comic object's
			for(int i = 0; i < n; i++)
			{
				comics[i] = (Comic)inputStream.readObject();
			}
			
            inputStream.close();
        }
        catch (ClassNotFoundException e)
        {
			System.out.println("ERROR: cannot read object from " + inputFileName);
			System.out.println("Ending program...");
            System.exit(0);
		}
        catch (FileNotFoundException e)
        {
            System.out.println("ERROR: cannot find file " + inputFileName);
            System.out.println("Ending program...");
            System.exit(0);
        }
        catch (IOException e)
        {
            System.out.println("ERROR: problem with file input from " + inputFileName);
            System.out.println("Ending program...");
            System.exit(0);
        }
		
		return n;
	}
	
	public static int inputObjectArrayData(String inputFileName, Comic[] comics)
	{
		//initializations to keep compiler happy
		int n = 0;
		ObjectInputStream inputStream = null;
		
		try
        {
            inputStream = new ObjectInputStream(new FileInputStream(inputFileName));
			Comic[] array;
			
			//OBJECT_ARRAY_DATA_FILE format:
			array = (Comic[])inputStream.readObject();
			n = array.length; //get size of array, only need to do once
			// array of Comic object's
			for(int i = 0; i < n; i++)
			{
				comics[i] = array[i];
			}

            inputStream.close();
        }
        catch (ClassNotFoundException e)
        {
			System.out.println("ERROR: cannot read object from " + inputFileName);
			System.out.println("Ending program...");
            System.exit(0);
		}
        catch (FileNotFoundException e)
        {
            System.out.println("ERROR: cannot find file " + inputFileName);
            System.out.println("Ending program...");
            System.exit(0);
        }
        catch (IOException e)
        {
            System.out.println("ERROR: problem with file input from " + inputFileName);
            System.out.println("Ending program...");
            System.exit(0);
        }
		
		return n;
	}
	
	
	
	//HELPER METHODS
	public static int readInt(String prompt, Scanner key, int lower, int upper)
	{
		String temp;
		int result;
		boolean isNotValid;
		
		result = 0;
		isNotValid = true; //keep compiler happy

		do
		{
			System.out.print(prompt);
			temp = key.nextLine();
			try
			{
				result = Integer.parseInt(temp);
				isNotValid = (result < lower) || (result > upper);
			
				if(isNotValid)
				{
					System.out.println("ERROR: please enter value between " + lower + " - " + upper);
				}
			}
			catch (NumberFormatException nfe)
			{
				System.out.println("ERROR: must enter integer (whole number).");
			}
			
			
		} while(isNotValid);

		return result;
	}
	
	public static void printArray(int numComics, Comic[] comics)
	{
		for(int i = 0; i < numComics; i++)
		{
			System.out.println("Comic #" + (i+1) + ": " + comics[i]);
		}
	}
	
	public static void printArray(int numComics, int[] issueNums,
				String[] names, char[] publishers, double[] ratings)
	{
		for(int i = 0; i < numComics; i++)
		{
			System.out.printf("Comic #%d: %s #%d (%s), Rating: %.1f/%.1f%n",
			i+1, names[i], issueNums[i], publishers[i], ratings[i], Comic.RATING_MAX);
		}
	}
}