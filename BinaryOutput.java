/* 
//BASIC_DATA_FILE format:
	// 1 integer representing number of each type of data file has (n)
	// n integer's representing comic issue number
	// n string's representing comic name
	// n char's representing comic publisher
	// n double's representing comic quality rating
//ARRAY_DATA_FILE format:
	// array of integer's representing comic issue numbers
	// array of string's representing comic names
	// array of char's representing comic publishers
	// array of double's representing comic quality ratings
//OBJECT_DATA_FILE format:
	// 1 integer representing number of comic objects file has (n)
	// n Comic object's
//OBJECT_ARRAY_DATA_FILE format:
	// array of Comic object's
*/

import java.util.Scanner;

public class BinaryOutput
{
	public static final String BASIC_DATA_FILE = "basicData.dat";
	public static final String ARRAY_DATA_FILE = "arrayData.dat";
	public static final String OBJECT_DATA_FILE = "objectData.dat";
	public static final String OBJECT_ARRAY_DATA_FILE = "objectArrayData.dat";

	/* ALGORITHM:
	 * 
	 * DO
	 * 		INTIALIZE array of comics
	 * 		PROMPT menu for how to output file
	 * 		READ choice
	 * 		SWITCH choice
	 * 			CASE 0
	 * 				OUTPUT array of comics
	 * 				BREAK
	 * 			CASE 1
	 * 				CALL outputBasicData()
	 * 				BREAK
	 *			CASE 2
	 * 				CALL outputArrayData()
	 * 				BREAK
	 * 			CASE 3
	 * 				CALL outputObjectData()
	 * 				BREAK
	 * 			CASE 4
	 * 				CALL outputObjectArrayData()
	 * 				BREAK
	 * 			CASE 5
	 * 				OUTPUT exit message
	 * 				BREAK
	 * 		END SWITCH
	 * 		OUTPUT "Done"
	 * WHILE choice != 5
	 * 
	 */
    public static void main(String[] args)
    {
		//DECLARATIONS + INITIALIZATIONS
		Scanner keyboard = new Scanner(System.in);
		String fileName = ""; //keep compiler happy
		int choice;
		Comic[] collection = {
				new Comic("Ms. Marvel", 1, 'M', 9.8),
				new Comic("Amazing Spider-Man", 121, 'M', 1.3),
				new Comic("Batman Beyond", 1, 'D', 9.4),
				new Comic("Watchmen", 1, 'V', 3.2),
				new Comic("Manhattan Projects", 32, 'I', 8.8)
			};
		
		System.out.println("Welcome to Binary File Output!");
		
		do
		{
			//INPUT
			System.out.println("\n\n");
			System.out.println("We have a bunch of data, choose how its outputted.");
			System.out.println("0: Show data in program");
			System.out.println("1: Output basic data file (primitives + Strings)");
			System.out.println("2: Output array data file (arrays of primitives + Strings)");
			System.out.println("3: Output object data file (objects)");
			System.out.println("4: Output object array data file (array of objects)");
			System.out.println("5: Exit program");
			choice = BinaryInput.readInt("Enter choice: ", keyboard, 0, 5);
			
			//PROCESSING + (console/file)OUTPUT
			System.out.println();
			
			switch(choice)
			{
				case 0:
					BinaryInput.printArray(collection.length, collection);
					break;
				case 1:
					outputBasicData(BASIC_DATA_FILE, collection);
					System.out.println("Data outputted to " + BASIC_DATA_FILE);
					break;
				case 2:
					outputArrayData(ARRAY_DATA_FILE, collection);
					System.out.println("Data outputted to " + ARRAY_DATA_FILE);
					break;
				case 3:
					outputObjectData(OBJECT_DATA_FILE, collection);
					System.out.println("Data outputted to " + OBJECT_DATA_FILE);
					break;
				case 4:
					outputObjectArrayData(OBJECT_ARRAY_DATA_FILE, collection);
					System.out.println("Data outputted to " + OBJECT_ARRAY_DATA_FILE);
					break;
				case 5:
					System.out.println("Thanks for using our program! See you soon!");
					break;
				//no default needed, readInt() guarantees we get a value 0-5
			}
			
			System.out.println();
			
		} while (choice != 5);
		
		keyboard.close(); //housekeeping
    }
    
    
    
    //BINARY FILE OUTPUT METHODS
    public static void outputBasicData(String outputFileName, Comic[] comics)
    {
		//TODO: output BASIC_DATA_FILE following correct format
	}
	
	public static void outputArrayData(String outputFileName, Comic[] comics)
    {
		//TODO: output ARRAY_DATA_FILE following correct format
			//HINT: good idea to create the 4 arrays before outputting
	}
	
	public static void outputObjectData(String outputFileName, Comic[] comics)
    {
		//TODO: output OBJECT_DATA_FILE following correct format
	}
	
	public static void outputObjectArrayData(String outputFileName, Comic[] comics)
    {
		//TODO: output OBJECT_ARRAY_DATA_FILE following correct format
	}
	
}