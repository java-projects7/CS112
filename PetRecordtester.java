//import java.io.Fileprinter;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.io.IOException;
import java.util.Scanner;


public class PetRecordtester
{
	public static void main (String[] args)
	{
		Scanner input = new Scanner(System.in);
		
		
		// example code PetRecord pet1 = new PetRecord("fiddo", 10, 22.2);
		
        
		
		
		
		
		// Example of Creating a user input array to create as many pets as they want
		System.out.println("How many pets do you want to Store ");
		int petStorage= input.nextInt();
		PetRecord[] pets= new PetRecord[petStorage];
		
		input.nextLine();
		
		//Example of filling in an object parameters using for Loop
		for(int i =0; i < petStorage; i++)
		{
		
		System.out.println("Please enter A pet name");
		String name = input.nextLine();
		
		
		System.out.println("please enter pet age");
		int age= input.nextInt();
	
		
		System.out.println("Please enter pet weight");
		double weight = input.nextDouble();
		
		input.nextLine();
		
		pets[i] = new PetRecord(name, age, weight);
		
		
		}
		
		PetRecord largestPet = pets[0];
		PetRecord smallestPet = pets[0];
		PetRecord youngestPet = pets[0];
		PetRecord oldestPet = pets[0];
		
		
		//A FOR loop array to find largest and smallest pet 
		for(int i = 1 ; i < pets.length; i++)
		{
				if(pets[i].getWeight() > largestPet.getWeight())
				{
					largestPet= pets[i];
					System.out.println("Largest Pet on record is: " + pets[i]);
				}
				if(pets[i].getWeight() < smallestPet.getWeight())
				{
					smallestPet = pets[i];
					System.out.println("Smallest pet on record is: " + pets[i]);
				}
		}
		
		//A FOR loop array to find oldest and youngest pet
		for(int i = 1 ; i < pets.length; i++)
		{
				if(pets[i].getAge() > oldestPet.getAge())
				{
					oldestPet= pets[i];
					System.out.println("Oldest Pet on record is: " + pets[i]);
				}
				if(pets[i].getAge() < youngestPet.getAge())
				{
					youngestPet = pets[i];
					System.out.println("Youngest pet on record is: " + pets[i]);
				}
		}
		
		
		
		
		
		
		
		
		try
        {
			
        
        ObjectOutputStream file = new ObjectOutputStream(new FileOutputStream("output.dat"));

        file.writeObject(pets);
        file.close();
		}
		catch (IOException e)
		{
		System.out.println("");
		}  
		
		
        
	}
}


